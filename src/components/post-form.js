import React, { Component } from 'react';

export class PostForm extends Component {
  state = {
    title: '',
    text: '',
  };

  onChange = ({ target: { name, value } }) => this.setState({ [name]: value });

  onSearchChange = ({ target: { value } }) => {
    this.setState({ value });
    this.props.applySearch(value);
  };

  onAddPost = () => {
    const { addPost } = this.props;
    const { title, text } = this.state;
    addPost({ title, text, id: Math.random().toString(36).substr(2, 9) });
  };

  onSearchChange = (ev) => {
    this.props.onSearchChange(ev.target.value);
  };

  render() {
    const { title, text } = this.state;
    const {
      toggleStyles,
      postTitleRef,
      postTextRef,
      applySearch,
      searchValue,
    } = this.props;
    return (
      <div>
        <input
          ref={postTitleRef}
          className="new-post-title"
          type="text"
          placeholder="Title"
          name="title"
          onChange={this.onChange}
          value={title}
        />
        <br />
        <br />
        <textarea
          ref={postTextRef}
          className="new-post-area"
          placeholder="Text"
          rows={5}
          name="text"
          onChange={this.onChange}
          value={text}
        />
        <br />
        <button onClick={this.onAddPost}>Add Post!</button>
        <button onClick={toggleStyles}>Toggle Styles</button>
        <hr />
        <h2>Search posts!</h2>
        <input
          type="text"
          placeholder="Search..."
          value={searchValue}
          onChange={this.onSearchChange}
        />
        <div>
          <button onClick={() => applySearch('title')}>Search by title</button>
          <button onClick={() => applySearch('text')}>Search by text</button>
        </div>
      </div>
    );
  }
}
