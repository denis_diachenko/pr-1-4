import React from 'react';
import './app.css';
import { PostForm } from './post-form';
import { Post } from './post';

import staticPosts from '../static/staticPosts.json';

export default class App extends React.Component {
  state = {
    isStyledToggled: false,
    searchValue: '',
    searchBy: null,
    posts: [ ...staticPosts ],
  };
  postTitleRef = React.createRef();
  postTextRef = React.createRef();


  addPost = (newPost) => {
    this.setState((prevState) => ({
      posts: [newPost, ...prevState.posts],
    }));
  };

  deletePost = (postIdToDelete) => {
    const { posts } = this.state;

    const nextPosts = posts.filter(
      ({ id }) => id !== postIdToDelete
    );
    this.setState({
      posts: nextPosts,
    });

  };

  toggleStyles = () => {
    this.setState((prev) => {
      if (prev.isStyledToggled) {
        this.postTitleRef.current.style.color = 'red';
        this.postTextRef.current.style.fontWeight = 'bold';
      } else {
        this.postTitleRef.current.style.color = 'black';
        this.postTextRef.current.style.fontWeight = 'normal';
      }
      return { isStyledToggled: !prev.isStyledToggled };
    });
  };

  get postsToShow() {
    const { searchValue, posts, searchBy } = this.state;
    if (!searchValue.trim() || !searchBy) {
      return posts;
    }

    return posts.filter((post) => {
      const val = post[searchBy];
      return val.toLowerCase().includes(searchValue.toLowerCase());
    });
  }

  applySearch = (searchBy) => {
    this.setState({ searchBy });
  };

  onSearchChange = (searchValue) => {
    this.setState({ searchBy: undefined, searchValue });
  };

  renderPosts = (post) => (
    <Post key={post.id} post={post} deletePost={this.deletePost} />
  )

  render() {
    const { searchValue } = this.state;
    return (
      <div>
        <h1>React components</h1>
        <PostForm
          addPost={this.addPost}
          toggleStyles={this.toggleStyles}
          postTitleRef={this.postTitleRef}
          postTextRef={this.postTextRef}
          searchValue={searchValue}
          applySearch={this.applySearch}
          onSearchChange={this.onSearchChange}
        />
        {this.postsToShow.map(this.renderPosts)}
      </div>
    );
  }
}
